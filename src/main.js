import { GameListView } from './GameListView.js';
import { Router } from './Router.js';
import { HelpView } from './HelpView.js';
import View from './View.js';

/* TP2 / B.2. Modifier des éléments */
// Activation du lien du menu
// const gameListLink = document.querySelector('.mainMenu .gameListLink');
// gameListLink.setAttribute(
// 	'class',
// 	gameListLink.getAttribute('class') + ' active'
// );
// ou
document.querySelector('.mainMenu .gameListLink').classList.add('active');
// Affichage du titre h1
// document.querySelector('.viewTitle').innerHTML = '<h1>MAGASIN</h1>';
// Modification du footer
document.querySelector('body > footer > div:nth-of-type(2)').innerHTML +=
	' / CSS inspirée de <a href="https://store.steampowered.com/">steam</a>';
// ou
// document.querySelectorAll('body > footer > div')[1].innerHTML +=
// 	' / CSS inspirée de <a href="https://store.steampowered.com/">steam</a>';

/* TP2 / C.3.4. Afficher la bonne page */
// On affiche la gameList par défaut
// document.querySelector('.gameList').classList.add('active');

/* TP2 / C.3. Navigation en JS : le menu */
/**
 * Fonction déclenchée au clic sur les liens du menu
 * Permet de mettre à jour le titre de la page (.viewTitle)
 * et d'afficher la vue correspondante (.viewContent)
 * @param {Event} event
 */
// on écoute le clic sur tous les liens du menu

/* TP2 / C.2. Afficher/masquer un élément */
// comme on aura besoin plusieurs fois de ce querySelector searchForm
// (cf. fonction toggleSearchForm) on le stocke dans une constante.
// C'est plus léger au niveau du code et c'est aussi plus performant
// (on ne scanne le document qu'une seule fois)
/**
 * Fonction qui permet d'afficher ou masquer le formulaire de recherche
 * @param {Event} event événement déclenché par le bouton sur lequel on a cliqué
 */

// Au clic sur le bouton "loupe" on appelle la fonction toggleSearchForm
// NB: on passe une "référence" vers la fonction (son nom) pour qu'elle
// puisse être appelée par le bouton quand on cliquera dessus,
// on ne l'exécute pas nous même c'est à dire qu'on ne met pas `toggleSearchForm()`

/**
 * Fonction d'affichage de la liste des jeux.
 * Utilisée :
 * - au chargement de la page pour l'affichage initial
 * - et à chaque fois qu'on soumet le formulaire de recherche
 * @param {string} search chaîne de caractère recherchée dans le nom des jeux
 * @param {string} ordering ordre d'affichage des résultats
 */

/* TP2 / D.2. Le formulaire de contact */
/**
 * Fonction déclenchée au submit du formulaire de la page "Support"
 * Vérifie si les champs sont correctement renseignés et lance la rédaction
 * d'un email prérempli avec les infos saisies par l'utilisateur.rice
 * @param {Event} event
 */
// on écoute la soumission du formulaire de contact
// const helpForm = document.querySelector('.helpForm');
// helpForm.addEventListener('submit', handleHelpFormSubmit);
const helpView = new HelpView(document.querySelector('.viewContent .help'));
const gameListView = new GameListView(
	document.querySelector('.viewContent > .gameList')
);
gameListView.renderGameList();
const aboutView = new View(document.querySelector('.viewContent > .about'));
const routes = [
	{ path: '/', view: gameListView, title: 'Magasin' },
	{ path: '/about', view: aboutView, title: 'À propos' },
	{ path: '/help', view: helpView, title: 'Support' },
];
Router.routes = routes;
Router.setMenuElement(document.querySelector('.mainMenu'));
Router.navigate('/' + window.location.href.split('/').slice(-1), false);

window.onpopstate = event => {
	Router.navigate(event.state == null ? '/' : event.state, false);
};
