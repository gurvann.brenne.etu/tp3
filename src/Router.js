function handleMenuLinkClick(event) {
	event.preventDefault();
	const linkHref = event.currentTarget.getAttribute('href');
	Router.navigate(linkHref);
}

class Router {
	static routes;
	static titleElement;
	static #menuElement;
	static navigate(path, addToHistory = true) {
		const activatedLink = this.#menuElement.querySelector('a.active');
		if (activatedLink != null) {
			activatedLink.classList.remove('active');
		}

		Router.routes.forEach(elm => {
			elm.view.hide();
		});

		const toNav = Router.routes.find(element => element.path == path);
		if (toNav != null) {
			// Empâcher le retour en arrière sur rien
			document.querySelector(
				'.viewTitle'
			).innerHTML = `<h1>${toNav.title}</h1>`;
			this.#menuElement
				.querySelector('.mainMenu a[href="' + path + '"]')
				.classList.add('active');
			toNav.view.show();
			if (addToHistory) {
				window.history.pushState(path, '', path);
			}
		}
	}
	static setMenuElement(element) {
		this.#menuElement = element;
		this.#menuElement.querySelectorAll('a').forEach(link => {
			link.addEventListener('click', handleMenuLinkClick);
		});
	}
}

export { Router };
