import View from './View.js';

class HelpView extends View {
	constructor(elm) {
		super(elm);
		this.element.addEventListener('submit', event => this.handleSubmit(event));
	}
	handleSubmit(event) {
		event.preventDefault();
		// récupération des 2 champs du formulaire
		const subjectInput = this.element.querySelector('input[name=subject]');
		const bodyTextarea = this.element.querySelector('textarea[name=body]');
		// récupération du texte saisi par l'utilisateur.rice
		const subject = subjectInput.value,
			body = bodyTextarea.value;
		// vérification des champs obligatoires
		if (subject === '') {
			alert('le champ "SUJET" est obligatoire');
			return;
		}
		if (body === '') {
			alert('le champ "VOTRE MESSAGE" est obligatoire');
			return;
		}
		// ouverture du client mail pour rédaction d'un email
		window.location.href = `mailto:help@jsteam.fr?subject=${encodeURIComponent(
			subject
		)}&body=${encodeURIComponent(body)}`;
		// on vide les champs
		subjectInput.value = '';
		bodyTextarea.value = '';
		// on aurait aussi pu faire : form.reset();
	}
}

const helpView = new HelpView(document.querySelector('.viewContent .help'));

export { HelpView };
