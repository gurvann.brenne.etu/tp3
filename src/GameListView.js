import { renderGameThumbnail } from './renderGameThumbnail.js';
import data from './data.js';
import View from './View.js';

// function renderGameList(search = '', ordering) {
// 	// calcul de la fonction de tri selon le paramètre ordering
// 	let sortingFunction;
// 	switch (ordering) {
// 		case '-metacritic':
// 			sortingFunction = (a, b) => b.metacritic - a.metacritic;
// 			break;
// 		case '-released':
// 			sortingFunction = (a, b) => b.released.localeCompare(a.released);
// 			break;
// 	}
// 	// parcours du tableau + génération du code HTML de la gameList
// 	let html = '';
// 	data
// 		.filter(game => game.name.toLowerCase().includes(search.toLowerCase())) // recherche
// 		.sort(sortingFunction) // tri
// 		.forEach(name => (html += renderGameThumbnail(name))); // génération du HTML
// 	// maj de la page HTML
// 	document.querySelector('.gameList .results').innerHTML = html;
// }

// const toggleSearchButton = document.querySelector(
// 	'.gameList .toggleSearchButton'
// );
// const searchForm = document.querySelector('.gameList .searchForm');

// function toggleSearchForm(event) {
// 	console.log('on a cliqué sur le bouton de recherche');
// 	const isOpened = searchForm.getAttribute('style') !== 'display: none;';
// 	if (!isOpened) {
// 		searchForm.setAttribute('style', '');
// 		toggleSearchButton.classList.add('opened');
// 	} else {
// 		searchForm.setAttribute('style', 'display: none;');
// 		toggleSearchButton.classList.remove('opened');
// 	}
// }

// function handleSearchFormSubmit(event) {
// 	event.preventDefault();
// 	const searchInput = searchForm.querySelector('[name=search]'),
// 		orderingSelect = searchForm.querySelector('[name=ordering]');
// 	renderGameList(searchInput.value, orderingSelect.value);
// }

// toggleSearchButton.addEventListener('click', toggleSearchForm);
// searchForm.addEventListener('submit', handleSearchFormSubmit);

class GameListView extends View {
	toggleSearchButton;
	searchForm;
	constructor(element) {
		super(element);
		this.toggleSearchButton = this.element.querySelector('.toggleSearchButton');
		this.searchForm = this.element.querySelector('.searchForm');

		this.toggleSearchButton.addEventListener('click', event =>
			this.toggleSearchForm(event)
		);
		this.searchForm.addEventListener('submit', event =>
			this.handleSearchFormSubmit(event)
		);
	}
	renderGameList(search = '', ordering) {
		// calcul de la fonction de tri selon le paramètre ordering
		let sortingFunction;
		switch (ordering) {
			case '-metacritic':
				sortingFunction = (a, b) => b.metacritic - a.metacritic;
				break;
			case '-released':
				sortingFunction = (a, b) => b.released.localeCompare(a.released);
				break;
		}
		// parcours du tableau + génération du code HTML de la gameList
		let html = '';
		data
			.filter(game => game.name.toLowerCase().includes(search.toLowerCase())) // recherche
			.sort(sortingFunction) // tri
			.forEach(name => (html += renderGameThumbnail(name))); // génération du HTML
		// maj de la page HTML
		this.element.querySelector('.results').innerHTML = html;
	}
	toggleSearchForm(event) {
		console.log('on a cliqué sur le bouton de recherche');
		const isOpened = this.searchForm.getAttribute('style') !== 'display: none;';
		if (!isOpened) {
			this.searchForm.setAttribute('style', '');
			this.toggleSearchButton.classList.add('opened');
		} else {
			this.searchForm.setAttribute('style', 'display: none;');
			this.toggleSearchButton.classList.remove('opened');
		}
	}

	handleSearchFormSubmit(event) {
		event.preventDefault();
		const searchInput = this.searchForm.querySelector('[name=search]'),
			orderingSelect = this.searchForm.querySelector('[name=ordering]');
		this.renderGameList(searchInput.value, orderingSelect.value);
	}
}

export { GameListView };
